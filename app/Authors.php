<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Authors extends Model
{

    /**
     * 
     */
    public function articles()
    {
        return $this->hasOne('App\Articles');
    }

    public function phones()
    {
        return $this->hasOne('App\Phones');
    }
}
 