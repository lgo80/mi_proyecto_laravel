<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ArticlesController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @return Illuminate\Http\Response;
     */
    public function index($id = null)
    {

        return ($id) ? "El id es: " . $id : "entrando al index";
    }

    /**
     * Show the profile for the given user.
     *
     * @return Illuminate\Http\Response;
     */
    public function create()
    {
        return view("Articles.create", ["user" => "Leito"]);
    }

    /**
     * Show the profile for the given user.
     *
     * @return Illuminate\Http\Response;
     */
    public function delete()
    {
        return "Cargando metodo delete";
    }

    /**
     * Show the profile for the given user.
     *
     * @return Illuminate\Http\Response;
     */
    public function modify()
    {
        return view("Articles.modify", ["user" => "Leito"]);
    }

    /**
     * Show the profile for the given user.
     *
     * @return Illuminate\Http\Response;
     */
    public function lista()
    {
        return view("Articles.lista", ["user" => "Leito"]);
    }
}
