<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Clients;

class ClientController extends Controller
{

    /**
     * 
     */
    public function index()
    {
        $clientsObject = Clients::all();
        //$clients = $clientObject->all();
        $user = (Auth::check()) ? Auth::user()->name : 'invitado';
        return view('sidebarView', ['clients' => $clientsObject, 'user' => $user]);
    }

    /**
     * Show the profile for the given user.
     *
     * @return Illuminate\Http\Response;
     */
    public function create()
    {

        $clientElegido = [
            "name" => null,
            "email" => null
        ];
        return view('add', $clientElegido);
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:25|min:3',
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            return redirect('client/create')
                ->withErrors($validator)
                ->withInput();
        }

        $client = new Clients;

        $client->name = $request->name;
        $client->email = $request->email;

        $client->save();
        /*Clients::create([
            "name" => $request->name,
            "email" => $request->email
        ]);*/
        return redirect('sidebarView');
    }

    /**
     * Show the profile for the given user.
     *
     * @return Illuminate\Http\Response;
     */
    public function show($id)
    {

        $clientElegido = Clients::find($id);
        return view('add', [
            "name" => $clientElegido->name,
            "email" => $clientElegido->email
        ]);
    }

    /**
     * Show the profile for the given user.
     *
     * @return Illuminate\Http\Response;
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|min:4',
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return redirect('/details/{$id}')
                ->withErrors($validator)
                ->withInput();
        }

        Clients::where('id', $id)
            ->update([
                "name" => $request->input('name'),
                "email" => $request->input('email')
            ]);
        return redirect('/sidebarView');
    }

    /**
     * Show the profile for the given user.
     *
     * @return Illuminate\Http\Response;
     */
    public function destroy($id)
    {

        Clients::destroy($id);
        return redirect('/sidebarView');
    }
}
