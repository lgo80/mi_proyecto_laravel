<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Articles extends Model
{

    protected $guarded = ['name', 'text'];
    /**
     * 
     */
    public function setNewName($name)
    {

        $this->name = $name;
        $this->save();
    }

    public function authors()
    {
        return $this->belongsTo('App\Authors');
    }
}
