<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;
use App\Articles;
/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('greet {user} {nombre}', function () {
    $user = $this->argument("user");
    $nombre = $this->argument("nombre");
    $first_article = Articles::first();
    $this->comment($first_article->name . " - " . $user . " - " . $nombre); //Inspiring::quote()
})->describe('Display an inspiring quote');
