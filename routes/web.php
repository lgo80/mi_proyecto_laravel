<?php

use App\Client;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\URL;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('mensaje', function () {
    return "Hola mundo";
});
Route::get('/', function () {
    return view('inicial');
});

Route::get('/index', function () {
    $user = (Auth::check()) ? Auth::user()->name : 'invitado';
    return view('foundation', ["user" => $user]);
});

Route::get('form', function () {
    return view('form');
});

Route::post('form', function () {
    return "Cargando la vista del POST";
});

Route::get('greet/{id}', function ($name) {
    return "Hola " . $name;
});

Route::get('vuelve', function () {
    return redirect("/");
});

Route::get('user/profile', array('as' => 'profile'), function () {
    return "ruta nombrada";
});

Route::get('get/named_url', function () {
    return URL::route('profile');
});

Route::get('articles', "ArticlesController@index");
Route::get('articles/{id}', "ArticlesController@index");

Route::get('articlesCreate', [
    'uses' => "ArticlesController@create",
    'as' => 'crearArticle', 'user' => 'Leito'
]);

Route::get('articlesModify', 'ArticlesController@modify');

Route::resource('users2', 'users2Controller');

Route::get('articlesLista', 'ArticlesController@lista');

Route::match(['get', 'post'], '/nueva', function (Request $request) {
    $txtCorreo = $request->input('txtCorreo');
    $txtIdioma = $request->input('txtIdioma');
    $chkNoticias = $request->input('chkNoticias');
    $input = $request->all();

    return view('nueva', [
        'txtCorreo' => $txtCorreo,
        'txtIdioma' => $txtIdioma,
        'chkNoticias' => $chkNoticias
    ]);
});

Route::get('sidebarView', 'ClientController@index');

/*Route::get('add', function (Request $request) {
    $request->flash();
    $data = [];
    $data['name'] = "";
    $data['email'] = "";
    return view('detail', $data);
});

Route::post('add', function (Request $request) {
    $request->flash();
    return view('add', ['post' => true]);
});*/

Route::get('add', 'ClientController@create');

Route::post('add', 'ClientController@store');

Route::get('details/{id}', 'ClientController@show');

Route::post('details/{id}', 'ClientController@update');
Route::get('details/destroy/{id}', 'ClientController@destroy');

/*Route::get('details', function (Request $request) {
    $clientObject = new App\User;
    $clients = $clientObject->find($request->get('id'));
    return view('detail', $clients);
});

Route::post('details', function (Request $request) {
    return view('detail', ['post' => true]);
});*/

Route::get('client/create', 'ClientController@create');

Route::post('client/create', 'ClientController@store');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('logout', function () {
    Auth::logout();
    return redirect('/index');
});

Route::get('/security', function () {
    $articles = App\articles::find(4)->text;
    return view('security', ['injestion_attempt' => $articles, "prueba" => $articles]);
});
