<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'Hello' => 'Hello',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'Home' => 'Home',
    'User List' => 'User List',
    'Login' => 'Login',
    'New user' => 'New user',
    'Logout' => 'Log out',
    'New Client' => 'New Client',
];
