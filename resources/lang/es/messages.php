<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'Hello' => 'Hola',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'Home' => 'Inicio',
    'User List' => 'Lista Usuarios',
    'Login' => 'Loguearse',
    'New user' => 'Nuevo usuario',
    'Logout' => 'Cerrar sesion',
    'New Client' => 'Nuevo Cliente',
];
