@extends('layouts.master')

@section('content')
<div class="row">

  @include('layouts.sidebar')

  <div class="large-9 columns">

    <div class="section-container tabs" data-section>
      <section class="section">
        <h5 class="title"><a href="#panel1"> {{ $user or 'invitado' }}
          </a></h5>
        <div class="large-6 columns">
          <table width="100%">
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Correo</th>
                <th colspan=2>Acción</th>
              </tr>
            </thead>
            <tbody>
              @foreach($clients as $client)
              <tr>
                <td>{{ $client['name'] }}</td>
                <td>{{ $client['email'] }}</td>
                <td><a href="{{url('details/')}}/{{$client->id}}" class="button tiny radius secondary">MODIFICAR</a></td>
                <td><a href="{{url('details/destroy/')}}/{{$client->id}}" class="button tiny radius danger">ELIMINAR</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        @unless ( isset($user) )
        @include('layouts.sidebar_right')
        @endunless
      </section>
    </div>
  </div>

</div>
@stop