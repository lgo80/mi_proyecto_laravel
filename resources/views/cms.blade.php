@extends('layouts.master')

@section('content')

<div class="row">

	{{--
	@if ( $errors->any() )
	<div data-alert class="alert-box alert radius">
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
	@endforeach
	<a href="#" class="close">&times;</a>
</div>
@endif
--}}



@if( isset( $post ) )
<div data-alert class="alert-box success radius">
	Datos guardados
	<a href="#" class="close">&times;</a>
</div>
@endif

@include('partials.form')

</div>
@stop