@extends('layouts.master')

@section('content')
<div class="row">
    <div class="large-9 columns">
        <div class="section-container tabs" data-section>
            <section class="section">
                <h5 class="title"><a href="#panel1">Holas</a></h5>
                <div class="content" data-slug="panel1">
                    <form method="POST">
                        @csrf
                        <div class="row">
                            <div class="large-12 columns">
                                <label for="txtCorreo">
                                    Correo:
                                </label>
                                <input type="text" name="txtCorreo" value="{{ $txtCorreo }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-12 columns">
                                <label for="txtIdioma">
                                    Idioma:
                                </label>
                                <select name="txtIdioma">
                                    <option value="en" @if ($txtIdioma='en' ) selected @endif>English</option>
                                    <option value="fr" @if ($txtIdioma='fr' ) selected @endif>français</option>
                                    <option value="es" @if ($txtIdioma='es' ) selected @endif>Español</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-12 columns">
                                <label for="chkNoticias">
                                    ¿Desea recibir noticias?
                                </label>
                                <input type="checkbox" name="chkNoticias" value="yes" @if ($chkNoticias) checked @endif /> Si
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-12 columns">
                                <input type="submit" class="button small success radius" value="Enviar" />
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
</div>
@stop