<form method="post">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="row">
        <div class="large-4 columns">
            <label>Nombre
                <input type="text" name="name" class="@error('name') is-invalid @enderror" value="{{ $name }}" />
            </label>
            @error('name')
            <small class="error">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="row">
        <div class="large-4 columns">
            <label>Correo
                <input type="text"  class="@error('email') is-invalid @enderror" name="email" value="{{ $email }}" />
            </label>
            @error('email')
            <small class="error">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="row">
        <div class="large-12 columns">
            <label>
                <input class="button small success radius" type="submit" value="ENVIAR" />
            </label>
        </div>
    </div>
</form>